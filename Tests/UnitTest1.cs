﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CTFG_ConversorMV;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;

namespace Tests
{
    [TestClass]
    public class UnitTest
    {
        Interfaz testInterfaz;
        LeerConfiguracion conf;
        CrearConfiguracion crear;
        [TestMethod]
        public void TestConstructorInterfaz()
        {
            testInterfaz = new Interfaz();

        }
        [TestMethod]
        public void TestConstructorLeerConfig()
        {
            testInterfaz = new Interfaz();
            conf = new LeerConfiguracion(testInterfaz);


        }
        [TestMethod]
        public void TestConstructorCrearConfig()
        {
            testInterfaz = new Interfaz();
            conf = new LeerConfiguracion(testInterfaz);
            crear = new CrearConfiguracion(conf,testInterfaz);
        }
        [TestMethod]
        public void TestLeerConfiguracion()
        {
            testInterfaz = new Interfaz();
            conf = new LeerConfiguracion(testInterfaz);
            crear = new CrearConfiguracion(conf, testInterfaz);

        }
        [TestMethod]
        public void TestConfiguracionInicial()
        {
            testInterfaz = new Interfaz();
            conf = new LeerConfiguracion(testInterfaz);
            crear = new CrearConfiguracion(conf, testInterfaz);
            conf.configuracionInicial();
        }
        [TestMethod]
        public void TestGetConfiguracionMV()
        {
            testInterfaz = new Interfaz();
            conf = new LeerConfiguracion(testInterfaz);
            crear = new CrearConfiguracion(conf, testInterfaz);
            conf.configuracionInicial();
            conf.getConfiguracion(@"C:\Users\anto\Documents\Virtual Machines\Ubuntu 64-bit\Ubuntu 64-bit.vmx");
        }
        [TestMethod]
        public void TestCrearCarpeta()
        {
            testInterfaz = new Interfaz();
            conf = new LeerConfiguracion(testInterfaz);
            crear = new CrearConfiguracion(conf, testInterfaz);
            conf.configuracionInicial();
            conf.getConfiguracion(@"C:\Users\anto\Documents\Virtual Machines\Ubuntu 64-bit\Ubuntu 64-bit.vmx");
            crear.crearCarpeta(conf.ConfigMV);
        }
        [TestMethod]
        public void TestConvertirVHDD()
        {
            testInterfaz = new Interfaz();
            conf = new LeerConfiguracion(testInterfaz);
            crear = new CrearConfiguracion(conf, testInterfaz);
            conf.configuracionInicial();
            conf.getConfiguracion(@"C:\Users\anto\Documents\Virtual Machines\Ubuntu 64-bit\Ubuntu 64-bit.vmx");
            crear.crearCarpeta(conf.ConfigMV);
            crear.convertirDiscosDuros(conf.ConfigMV);
        }
        [TestMethod]
        public void TestCrearMV()
        {
            testInterfaz = new Interfaz();
            conf = new LeerConfiguracion(testInterfaz);
            crear = new CrearConfiguracion(conf, testInterfaz);
            conf.configuracionInicial();
            conf.getConfiguracion(@"C:\Users\anto\Documents\Virtual Machines\Ubuntu 64-bit\Ubuntu 64-bit.vmx");
            crear.crearCarpeta(conf.ConfigMV);
            var VHDDS=crear.convertirDiscosDuros(conf.ConfigMV);
            Task.Factory.StartNew(() =>
            {
                crear.crearMV(conf.ConfigMV, VHDDS);
            });
        }
        [TestMethod]
        public void TestSetLog()
        {
            testInterfaz = new Interfaz();
            testInterfaz.SetLog("Hola");
        }
        [TestMethod]
        public void TestActualizarBarra()
        {
            testInterfaz = new Interfaz();
            testInterfaz.actualizarBarra("55");
        }
        [TestMethod]
        public void TestSetActivarBotones()
        {
            testInterfaz = new Interfaz();
            testInterfaz.SetActivarBotones(true);
        }
    }
}

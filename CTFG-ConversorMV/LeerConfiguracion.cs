﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTFG_ConversorMV
{
    public class LeerConfiguracion
    {
        private Interfaz interfaz;
        private string rutaConfigMVs;
        private Dictionary<string, JObject> config;

        private string nombreArchivo;
        private string so;
        private int ram = 1024;
        private string configVersion;
        private string hwVersion;
        private int numNucleos = 1;
        private List<string> rutasHDD;

        private JObject configMV;

        public LeerConfiguracion(Interfaz interfaz)
        {
            this.interfaz = interfaz;
        }

        public void configuracionInicial()
        {
            Config = new Dictionary<string, JObject>();
            string configuracion = "";
            string line;
            //Se procede a leer el archivo de configuracion
            StreamReader file = new StreamReader("Init.json");
            while ((line = file.ReadLine()) != null)
            {
                configuracion += line;
            }
            var jsonConfig = JObject.Parse(configuracion);
            var VMs = jsonConfig["VM"].ToArray();
            interfaz.SelectHypervisor.Items.AddRange(VMs);
            if (jsonConfig["Dir"]!=null) {
                rutaConfigMVs = jsonConfig["Dir"].ToString();
            }else
            {
                rutaConfigMVs = "";
            }

            foreach (var vm in VMs)
            {
                Config.Add(vm.ToString(), leerConfiguracionMVs(rutaConfigMVs + vm + ".json"));

            }
            interfaz.SelectHypervisor.SelectedIndex = 0;

        }

        private JObject leerConfiguracionMVs(string rutaConfigMV)
        {
            string configuracion = "";
            string line;
            //Se procede a leer el archivo de configuracion de cada MV
            StreamReader file = new StreamReader(rutaConfigMV);
            while ((line = file.ReadLine()) != null)
            {
                configuracion += line;
            }
            var jsonConfig = JObject.Parse(configuracion);

            return jsonConfig;
        }


        public void getConfiguracion(string ruta)
        {
            RutasHDD = new List<string>();
            foreach (KeyValuePair<string, JObject> Par in config)
            {
                if (Path.GetExtension(ruta).ToLower().Equals(Par.Value["configExt"].ToString()))
                {
                    ConfigMV = Par.Value;
                    break;
                }
            }

            string line;
            //Obtenemos el nombre del archivo sin extension
            NombreArchivo = Path.GetFileNameWithoutExtension(ruta);
            //Se procede a leer el archivo
            StreamReader file = new StreamReader(ruta);
            while ((line = file.ReadLine()) != null)
            {
                if (containsAll(line, ConfigMV["SO"].ToArray()))
                {
                    SO = getValor(line, ConfigMV["getSO"].ToArray());
                    
                }
                if (containsAll(line, ConfigMV["RAM"].ToArray()))
                {
                    RAM = int.Parse(getValor(line, ConfigMV["getRAM"].ToArray()));

                }
                if (containsAll(line, ConfigMV["numCPU"].ToArray()))
                {
                    NumNucleos = int.Parse(getValor(line, ConfigMV["getNumCPU"].ToArray()));

                }
                if (containsAll(line, ConfigMV["HDD"].ToArray()))
                {
                    var str = Path.GetDirectoryName(ruta) + "\\" + Path.GetFileName(getValor(line, ConfigMV["getHDD"].ToArray()));
                    RutasHDD.Add(str.Replace("\\ ", "\\").Replace(" \\", "\\"));
                }
            }

            file.Close();
        }

        private string getValor(string line, JToken[] delimitadores)
        {
            string valor = line;
            foreach (var delim in delimitadores)
            {
                valor=valor.Split(new string[] { delim["palabra"].ToString() }, StringSplitOptions.None)[int.Parse(delim["posSplit"].ToString())];
            }
            return valor;
        }

        private bool containsAll(string line, JToken[] jToken)
        {
            bool loContiene = true;
            foreach (var palabra in jToken)
            {
                if (!line.Contains(palabra.ToString()))
                {
                    loContiene = false;
                    break;
                }
            }
            return loContiene;

        }

        public Dictionary<string, JObject> Config
        {
            get
            {
                return config;
            }

            set
            {
                config = value;
            }
        }

        public string NombreArchivo
        {
            get
            {
                return nombreArchivo;
            }

            set
            {
                nombreArchivo = value;
            }
        }

        public string SO
        {
            get
            {
                return so;
            }

            set
            {
                so = value;
            }
        }

        public int RAM
        {
            get
            {
                return ram;
            }

            set
            {
                ram = value;
            }
        }

        public string ConfigVersion
        {
            get
            {
                return configVersion;
            }

            set
            {
                configVersion = value;
            }
        }

        public string HwVersion
        {
            get
            {
                return hwVersion;
            }

            set
            {
                hwVersion = value;
            }
        }

        public int NumNucleos
        {
            get
            {
                return numNucleos;
            }

            set
            {
                numNucleos = value;
            }
        }

        public List<string> RutasHDD
        {
            get
            {
                return rutasHDD;
            }

            set
            {
                rutasHDD = value;
            }
        }

        public JObject ConfigMV
        {
            get
            {
                return configMV;
            }

            set
            {
                configMV = value;
            }
        }
    }
}

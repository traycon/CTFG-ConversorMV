var class_c_t_f_g___conversor_m_v_1_1_interfaz =
[
    [ "Interfaz", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#abfc08f54fe1cdf3a7376e9c16f9e238b", null ],
    [ "actualizarBarra", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a9dfd499e8d90391adcec36d76caaa065", null ],
    [ "botonAtras_Click", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a3244fe781f35c297ebaa115199eea2a0", null ],
    [ "botonConvertir_Click", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a4fd63fd67590f0da42ee4265815ec154", null ],
    [ "botonSalir_Click", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a194d630b36904817ca5525f3f690d8f7", null ],
    [ "Dispose", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#ad091e36fda3ad6361b9579278d184dfa", null ],
    [ "getSOFinal", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#af239e7f02310f2785600c0bef6d11e27", null ],
    [ "habilitarBotones", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#aefb557ec842a24d6cf91d71adea302c3", null ],
    [ "InitializeComponent", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#ae74549fcaff0bbfee308378e6b6f77bb", null ],
    [ "log", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a18de36f99b75a53807c9f8590c3614e9", null ],
    [ "progresoBarra", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#acb708128df2cd3708dfc394f97cccc4e", null ],
    [ "selectArchivo_click", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a9b960bd0a20a0185908227f2db71b9f9", null ],
    [ "selectHypervisor_SelectedIndexChanged", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a8a7bf0e298ecb56cfd50d878a97036c3", null ],
    [ "SetActivarBotones", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a3ceb93acc42959f61dcc340d73f64558", null ],
    [ "SetLog", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a638b4a2bfb9514079e8d75c85e6e2963", null ],
    [ "SetProgressBarValue", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a2d6f67d797ca53d2315bc0a7a614b4dd", null ],
    [ "barraProgreso", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a65c56052b559c71573b05d54622bcc92", null ],
    [ "botonAtras", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#af9a6fa0cdd6baa0d954d363e7261ea50", null ],
    [ "botonConvertir", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a1be2fc43d23ddac062d854a6ded69441", null ],
    [ "botonSalir", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a083308ba3b275b84b2b691b3cf3ad896", null ],
    [ "components", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#aa03d684eed27a3c7927120fc4576d2e8", null ],
    [ "conf", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a891f9b5c4bb1bd329f7445cc9f027f31", null ],
    [ "crear", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a7bcbdff0a7ea553cbf75c1a511fae0cc", null ],
    [ "selectArchivo", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#aa1b2dc5414a4cfd520c9d3c5a5442d40", null ],
    [ "selectHypervisor", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a8b6ad76fdc86186b747e612373faca66", null ],
    [ "selectVersion", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a201bdb87bd83b641a4cbc8e18ef12479", null ],
    [ "textLog", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#ab83168a1dde6050e23cec7a092e79801", null ],
    [ "textoSelectHypervisor", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a82cf748be00728d107cc3ddd3576b6b9", null ],
    [ "textoSelectVersion", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a6dd0057c59f91277b10cfc5a32f386d2", null ],
    [ "textoVario", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#aafd8db44cda530561809907b47c504f0", null ],
    [ "BarraProgreso", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a5963134cc8a7aed222fb717bd9268363", null ],
    [ "BotonAtras", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a9299a6da4964e8f932a4735daa8d18a2", null ],
    [ "BotonConvertir", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#afb7b32fac44ac495772bf37b74844963", null ],
    [ "BotonSalir", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a151c69951c90dc1f0ba0ba56ce2c6e40", null ],
    [ "SelectArchivo", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a1267fd6918dceebad2cae14b34a8d642", null ],
    [ "SelectHypervisor", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a70db7d301597950d2187f9b7d24a05c2", null ],
    [ "SelectVersion", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a382e5aa3f01856137e03a6fa0061c205", null ],
    [ "TextLog", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#afccf8c61635b032ff4abf2dcd74c16d7", null ],
    [ "TextoSelectHypervisor", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a8dcfd7fcdf7872a47dee41e46a27b9a7", null ],
    [ "TextoSelectVersion", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#a53689c68694118bf7e265494dfd622a3", null ],
    [ "TextoVario", "d8/d82/class_c_t_f_g___conversor_m_v_1_1_interfaz.html#ae8a218e7e867afa39282c9c56a896092", null ]
];
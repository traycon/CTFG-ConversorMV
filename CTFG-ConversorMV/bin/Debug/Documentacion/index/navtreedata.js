var NAVTREE =
[
  [ "ConversorMV", "index.html", [
    [ "Paquetes", null, [
      [ "Paquetes", "namespaces.html", "namespaces" ]
    ] ],
    [ "Clases", "annotated.html", [
      [ "Lista de clases", "annotated.html", "annotated_dup" ],
      [ "Índice de clases", "classes.html", null ],
      [ "Jerarquía de la clase", "hierarchy.html", "hierarchy" ],
      [ "Miembros de las clases", "functions.html", [
        [ "Todo", "functions.html", null ],
        [ "Funciones", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ],
        [ "Propiedades", "functions_prop.html", null ]
      ] ]
    ] ],
    [ "Archivos", null, [
      [ "Lista de archivos", "files.html", "files" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html"
];

var SYNCONMSG = 'click en deshabilitar sincronización';
var SYNCOFFMSG = 'click en habilitar sincronización';
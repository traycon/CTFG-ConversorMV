var menudata={children:[
{text:"Página principal",url:"index.html"},
{text:"Paquetes",url:"namespaces.html",children:[
{text:"Paquetes",url:"namespaces.html"}]},
{text:"Clases",url:"annotated.html",children:[
{text:"Lista de clases",url:"annotated.html"},
{text:"Índice de clases",url:"classes.html"},
{text:"Jerarquía de la clase",url:"inherits.html"},
{text:"Miembros de las clases",url:"functions.html",children:[
{text:"Todo",url:"functions.html",children:[
{text:"a",url:"functions.html#index_a"},
{text:"b",url:"functions.html#index_b"},
{text:"c",url:"functions.html#index_c"},
{text:"d",url:"functions.html#index_d"},
{text:"e",url:"functions.html#index_e"},
{text:"g",url:"functions.html#index_g"},
{text:"h",url:"functions.html#index_h"},
{text:"i",url:"functions.html#index_i"},
{text:"l",url:"functions.html#index_l"},
{text:"m",url:"functions.html#index_m"},
{text:"n",url:"functions.html#index_n"},
{text:"o",url:"functions.html#index_o"},
{text:"p",url:"functions.html#index_p"},
{text:"r",url:"functions.html#index_r"},
{text:"s",url:"functions.html#index_s"},
{text:"t",url:"functions.html#index_t"}]},
{text:"Funciones",url:"functions_func.html",children:[
{text:"a",url:"functions_func.html#index_a"},
{text:"b",url:"functions_func.html#index_b"},
{text:"c",url:"functions_func.html#index_c"},
{text:"d",url:"functions_func.html#index_d"},
{text:"e",url:"functions_func.html#index_e"},
{text:"g",url:"functions_func.html#index_g"},
{text:"h",url:"functions_func.html#index_h"},
{text:"i",url:"functions_func.html#index_i"},
{text:"l",url:"functions_func.html#index_l"},
{text:"m",url:"functions_func.html#index_m"},
{text:"p",url:"functions_func.html#index_p"},
{text:"s",url:"functions_func.html#index_s"}]},
{text:"Variables",url:"functions_vars.html"},
{text:"Propiedades",url:"functions_prop.html"}]}]},
{text:"Archivos",url:"files.html",children:[
{text:"Lista de archivos",url:"files.html"}]}]}

var indexSectionsWithContent =
{
  0: "abcdefghilmnoprst",
  1: "cilp",
  2: "c",
  3: "acflprst",
  4: "abcdeghilmps",
  5: "bchinorst",
  6: "bchnrst"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "properties"
};

var indexSectionLabels =
{
  0: "Todo",
  1: "Clases",
  2: "Namespaces",
  3: "Archivos",
  4: "Funciones",
  5: "Variables",
  6: "Propiedades"
};


﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTFG_ConversorMV
{
    public class CrearConfiguracion
    {
        private LeerConfiguracion conf;
        private int ordenComando = 0;
        private int siguienteComando = -1;
        private Interfaz interfaz;

        public CrearConfiguracion(LeerConfiguracion conf, Interfaz interfaz)
        {
            this.conf = conf;
            this.interfaz = interfaz;
        }

        public void crearMV(JObject configMVFinal, List<string> rutasHDDFinal)
        {
            if (configMVFinal["path"] == null)
            {
                configuracionPorEscrito(conf.NombreArchivo, conf.SO, conf.RAM, conf.ConfigVersion, conf.HwVersion, conf.NumNucleos, rutasHDDFinal, configMVFinal);
            }
            else
            {
                configuracionPorComandos(conf.NombreArchivo, conf.SO, conf.RAM, conf.ConfigVersion, conf.HwVersion, conf.NumNucleos, rutasHDDFinal, configMVFinal);
            }
        }

        public List<string> convertirDiscosDuros(JObject configMVFinal)
        {
            //Se procede a Convertir los discos duros al formato destino
            List<string> rutasHDDFinal = new List<string>();

            foreach (var rutaDisk in conf.RutasHDD)
            {
                siguienteComando += 1;
                ejecutarComando("bin\\qemu-img.exe", "convert -p -f " + conf.ConfigMV["hddExt"].ToString() + " -O " + configMVFinal["hddExt"].ToString() + " \"" + rutaDisk + "\" \"" + configMVFinal["carpetaMVs"].ToString() + conf.NombreArchivo + "_CMV\\" + Path.GetFileNameWithoutExtension(rutaDisk) + "_CMV." + configMVFinal["hddExt"].ToString() + "\"", siguienteComando);
                rutasHDDFinal.Add(configMVFinal["carpetaMVs"].ToString() + conf.NombreArchivo + "_CMV\\" + Path.GetFileNameWithoutExtension(rutaDisk) + "_CMV." + configMVFinal["hddExt"].ToString());
            }
            //Se termina de convertir los discos duros al formato destino
            return rutasHDDFinal;
        }

        public void crearCarpeta(JObject configMVFinal)
        {
            if (!Directory.Exists(configMVFinal["carpetaMVs"].ToString() + conf.NombreArchivo + "_CMV"))
            {
                Directory.CreateDirectory(configMVFinal["carpetaMVs"].ToString() + conf.NombreArchivo + "_CMV");
            }
        }

        private void configuracionPorEscrito(string nombreArchivo, string SO, int RAM, string configVersion, string HwVersion, int numNucleos, List<string> rutasHDD, JObject configMVFinal)
        {
            int contador = 0;
            List<string> config = new List<string>();
            foreach (var linea in configMVFinal["create"].ToArray())
            {
                if (linea.ToString().Contains("<contador>"))
                {
                    foreach (var hdd in rutasHDD)
                    {
                        string aux = linea.ToString().Replace("<contador>", "" + contador).Replace("<RAM>", "" + RAM).Replace("<numCPU>", "" + numNucleos).Replace("<SO>", "" + SO).Replace("<configVersion>", "" + configVersion).Replace("<HwVersion>", "" + HwVersion).Replace("<HDD>", "" + hdd).Replace("<nombreArchivo>", nombreArchivo + "_CMV");
                        config.Add(aux);
                        contador++;
                    }
                }
                else
                {
                    string aux = linea.ToString().Replace("<RAM>", "" + RAM).Replace("<numCPU>", "" + numNucleos).Replace("<SO>", "" + SO).Replace("<configVersion>", "" + configVersion).Replace("<HwVersion>", "" + HwVersion).Replace("<nombreArchivo>", nombreArchivo + "_CMV");
                    config.Add(aux);
                }
            }

            using (StreamWriter outputFile = new StreamWriter(configMVFinal["carpetaMVs"].ToString() + nombreArchivo + "_CMV" + "\\" + nombreArchivo + "_CMV" + configMVFinal["configExt"].ToString()))
            {
                foreach (string line in config)
                {
                    interfaz.TextLog.AppendText("- "+line + "\n");
                    outputFile.WriteLine(line);
                }
            }

        }

        private void configuracionPorComandos(string nombreArchivo, string SO, int RAM, string configVersion, string HwVersion, int numNucleos, List<string> rutasHDD, JObject configMVFinal)
        {
            int contador = 0;
            List<string> config = new List<string>();
            foreach (var linea in configMVFinal["create"].ToArray())
            {
                if (linea.ToString().Contains("<contador>"))
                {
                    foreach (var hdd in rutasHDD)
                    {
                        string aux = linea.ToString().Replace("<contador>", "" + contador).Replace("<RAM>", "" + RAM).Replace("<numCPU>", "" + numNucleos).Replace("<SO>", "" + SO).Replace("<configVersion>", "" + configVersion).Replace("<HwVersion>", "" + HwVersion).Replace("<HDD>", "" + hdd).Replace("<nombreArchivo>", nombreArchivo + "_CMV");
                        siguienteComando += 1;
                        ejecutarComando(configMVFinal["path"].ToString(), aux, siguienteComando);
                        contador++;
                    }
                }
                else
                {
                    string aux = linea.ToString().Replace("<RAM>", "" + RAM).Replace("<numCPU>", "" + numNucleos).Replace("<SO>", "" + SO).Replace("<configVersion>", "" + configVersion).Replace("<HwVersion>", "" + HwVersion).Replace("<nombreArchivo>", nombreArchivo + "_CMV");
                    siguienteComando += 1;
                    ejecutarComando(configMVFinal["path"].ToString(), aux, siguienteComando);
                }
            }

        }

        private void ejecutarComando(string rutaPrograma, string comando, int orden)
        {

            Task.Factory.StartNew(() =>
            {
                while (ordenComando != orden) { }
                interfaz.SetLog(rutaPrograma + " " + comando + "\n");
                System.Diagnostics.Process process = new System.Diagnostics.Process();
                System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.UseShellExecute = false;
                startInfo.FileName = rutaPrograma;
                startInfo.Arguments = comando;
                startInfo.RedirectStandardOutput = true;
                startInfo.CreateNoWindow = true;
                process.StartInfo = startInfo;
                process.EnableRaisingEvents = true;
                process.Exited += new EventHandler(procesoTerminado);
                process.Start();
                while (!process.StandardOutput.EndOfStream)
                {

                    interfaz.actualizarBarra(process.StandardOutput.ReadLine());

                }
            });

        }

        private void procesoTerminado(object sender, System.EventArgs e)
        {
            if (interfaz.BarraProgreso.Value == 100 && siguienteComando == ordenComando)
            {
                interfaz.SetActivarBotones(true);
            }
            ordenComando++;

        }
    }
}

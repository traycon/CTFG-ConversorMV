﻿using System.Windows.Forms;

namespace CTFG_ConversorMV
{
    partial class Interfaz
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextoVario = new System.Windows.Forms.Label();
            this.SelectArchivo = new System.Windows.Forms.TextBox();
            this.TextoSelectHypervisor = new System.Windows.Forms.Label();
            this.SelectHypervisor = new System.Windows.Forms.ComboBox();
            this.SelectVersion = new System.Windows.Forms.ComboBox();
            this.TextoSelectVersion = new System.Windows.Forms.Label();
            this.BotonConvertir = new System.Windows.Forms.Button();
            this.BarraProgreso = new System.Windows.Forms.ProgressBar();
            this.BotonAtras = new System.Windows.Forms.Button();
            this.BotonSalir = new System.Windows.Forms.Button();
            this.TextLog = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.TextoVario.AutoSize = true;
            this.TextoVario.Location = new System.Drawing.Point(29, 36);
            this.TextoVario.Name = "label1";
            this.TextoVario.Size = new System.Drawing.Size(348, 13);
            this.TextoVario.TabIndex = 0;
            this.TextoVario.Text = "Seleccione el archivo de configuración de la MV (.vmx, .vbox, ...)";
            // 
            // textBox1
            // 
            this.SelectArchivo.Location = new System.Drawing.Point(32, 64);
            this.SelectArchivo.Name = "textBox1";
            this.SelectArchivo.Size = new System.Drawing.Size(428, 20);
            this.SelectArchivo.TabIndex = 1;
            this.SelectArchivo.Click += new System.EventHandler(this.selectArchivo_click);
            // 
            // label2
            // 
            this.TextoSelectHypervisor.AutoSize = true;
            this.TextoSelectHypervisor.Location = new System.Drawing.Point(29, 99);
            this.TextoSelectHypervisor.Name = "label2";
            this.TextoSelectHypervisor.Size = new System.Drawing.Size(61, 13);
            this.TextoSelectHypervisor.TabIndex = 2;
            this.TextoSelectHypervisor.Text = "Convertir a:";
            // 
            // comboBox1
            // 
            this.SelectHypervisor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SelectHypervisor.FormattingEnabled = true;
            this.SelectHypervisor.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SelectHypervisor.Location = new System.Drawing.Point(32, 127);
            this.SelectHypervisor.Name = "comboBox1";
            this.SelectHypervisor.Size = new System.Drawing.Size(121, 21);
            this.SelectHypervisor.TabIndex = 3;
            this.SelectHypervisor.SelectedIndexChanged += new System.EventHandler(this.selectHypervisor_SelectedIndexChanged);
            this.SelectHypervisor.BackColor = System.Drawing.Color.White;
            // 
            // comboBox2
            // 
            this.SelectVersion.Location = new System.Drawing.Point(270, 127);
            this.SelectVersion.Name = "comboBox2";
            this.SelectVersion.Size = new System.Drawing.Size(121, 21);
            this.SelectVersion.TabIndex = 0;
            this.SelectVersion.Visible = false;
            this.SelectVersion.BackColor = System.Drawing.Color.White;
            // 
            // label3
            // 
            this.TextoSelectVersion.AutoSize = true;
            this.TextoSelectVersion.Location = new System.Drawing.Point(267, 99);
            this.TextoSelectVersion.Name = "label3";
            this.TextoSelectVersion.Size = new System.Drawing.Size(94, 13);
            this.TextoSelectVersion.TabIndex = 4;
            this.TextoSelectVersion.Text = "Version (opcional):";
            this.TextoSelectVersion.Visible = false;
            // 
            // button1
            // 
            this.BotonConvertir.Location = new System.Drawing.Point(141, 185);
            this.BotonConvertir.Name = "button1";
            this.BotonConvertir.Size = new System.Drawing.Size(164, 42);
            this.BotonConvertir.TabIndex = 5;
            this.BotonConvertir.Text = "Convertir";
            this.BotonConvertir.UseVisualStyleBackColor = true;
            this.BotonConvertir.Click += new System.EventHandler(this.botonConvertir_Click);
            this.BotonConvertir.BackColor = System.Drawing.Color.White;
            // 
            // progressBar1
            // 
            this.BarraProgreso.Location = new System.Drawing.Point(32, 64);
            this.BarraProgreso.Name = "progressBar1";
            this.BarraProgreso.Size = new System.Drawing.Size(428, 32);
            this.BarraProgreso.Step = 1;
            this.BarraProgreso.TabIndex = 6;
            this.BarraProgreso.Visible = false;
            this.BarraProgreso.BackColor = System.Drawing.Color.White;
            // 
            // button2
            // 
            this.BotonAtras.Location = new System.Drawing.Point(419, 142);
            this.BotonAtras.Name = "button2";
            this.BotonAtras.Size = new System.Drawing.Size(75, 23);
            this.BotonAtras.TabIndex = 7;
            this.BotonAtras.Text = "Atras";
            this.BotonAtras.UseVisualStyleBackColor = true;
            this.BotonAtras.Visible = false;
            this.BotonAtras.Click += new System.EventHandler(this.botonAtras_Click);
            this.BotonAtras.BackColor = System.Drawing.Color.White;
            // 
            // button3
            // 
            this.BotonSalir.Location = new System.Drawing.Point(419, 204);
            this.BotonSalir.Name = "button3";
            this.BotonSalir.Size = new System.Drawing.Size(75, 23);
            this.BotonSalir.TabIndex = 8;
            this.BotonSalir.Text = "Finalizar";
            this.BotonSalir.UseVisualStyleBackColor = true;
            this.BotonSalir.Visible = false;
            this.BotonSalir.Click += new System.EventHandler(this.botonSalir_Click);
            this.BotonSalir.BackColor= System.Drawing.Color.White;
            // 
            // textArea
            // 
            this.TextLog.Location = new System.Drawing.Point(34, 117);
            this.TextLog.Name = "textArea";
            this.TextLog.ReadOnly = true;
            this.TextLog.Size = new System.Drawing.Size(379, 110);
            this.TextLog.TabIndex = 9;
            this.TextLog.Text = "";
            this.TextLog.Visible = false;
            // 
            // Interfaz
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 261);
            this.Controls.Add(this.TextLog);
            this.Controls.Add(this.BotonSalir);
            this.Controls.Add(this.BotonAtras);
            this.Controls.Add(this.BarraProgreso);
            this.Controls.Add(this.BotonConvertir);
            this.Controls.Add(this.TextoSelectVersion);
            this.Controls.Add(this.SelectVersion);
            this.Controls.Add(this.SelectHypervisor);
            this.Controls.Add(this.TextoSelectHypervisor);
            this.Controls.Add(this.SelectArchivo);
            this.Controls.Add(this.TextoVario);
            this.Name = "Interfaz";
            this.Text = "Conversor de Maquinas Virtuales";
            this.ResumeLayout(false);
            this.PerformLayout();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.BackColor = System.Drawing.Color.GhostWhite;
        }

        #endregion

        private System.Windows.Forms.Label textoVario;
        private System.Windows.Forms.TextBox selectArchivo;
        private System.Windows.Forms.Label textoSelectHypervisor;
        private System.Windows.Forms.ComboBox selectHypervisor;
        private System.Windows.Forms.ComboBox selectVersion;
        private System.Windows.Forms.Label textoSelectVersion;
        private System.Windows.Forms.Button botonConvertir;
        private System.Windows.Forms.ProgressBar barraProgreso;
        private System.Windows.Forms.Button botonAtras;
        private System.Windows.Forms.Button botonSalir;
        private System.Windows.Forms.RichTextBox textLog;

        
    }
}


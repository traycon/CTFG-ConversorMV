﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json.Linq;
using System.Diagnostics;
using System.Threading;

namespace CTFG_ConversorMV
{
    public partial class Interfaz : Form
    {
        private LeerConfiguracion conf;
        private CrearConfiguracion crear;

        delegate void progresoBarra(int progreso);
        delegate void log(string info);
        delegate void habilitarBotones(bool habilita);

        public Interfaz()
        {
            InitializeComponent();
            conf = new LeerConfiguracion(this);
            crear = new CrearConfiguracion(conf, this);
            conf.configuracionInicial();
        }

        private void selectArchivo_click(object sender, EventArgs e)
        {
            string ext = "";
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            foreach (KeyValuePair<string, JObject> Par in conf.Config)
            {
                ext += "*" + Par.Value["configExt"].ToString() + ";";
            }
            ext = ext.Substring(0, ext.Length - 1);

            openFileDialog1.Filter = "Virtual Machines(" + ext + ")|" + ext;
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                SelectArchivo.Text = openFileDialog1.FileName;
            }
        }

        private void selectHypervisor_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.TextoSelectVersion.Visible = false;
            this.SelectVersion.Visible = false;

            foreach (KeyValuePair<string, JObject> Par in conf.Config)
            {
                if (Par.Key.Equals(SelectHypervisor.Text))
                {
                    if (Par.Value["opcional"] != null)
                    {
                        this.TextoSelectVersion.Visible = true;
                        this.SelectVersion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
                        this.SelectVersion.FormattingEnabled = true;
                        this.SelectVersion.ImeMode = System.Windows.Forms.ImeMode.NoControl;
                        this.SelectVersion.Items.Clear();
                        this.SelectVersion.Items.AddRange(Par.Value["opcional"].ToArray());
                        this.SelectVersion.SelectedIndex = 0;
                        this.SelectVersion.Visible = true;
                        break;
                    }
                }
            }

        }

        private void botonConvertir_Click(object sender, EventArgs e)
        {
            if (SelectArchivo.Text != null && !SelectArchivo.Text.Equals(""))
            {
                if (SelectVersion.Visible == true)
                {
                    conf.ConfigVersion = "" + SelectVersion.Text.Split(new char[] { ' ' })[0].Split(new char[] { ':' })[1];
                    conf.HwVersion = "" + SelectVersion.Text.Split(new char[] { ' ' })[1].Split(new char[] { ':' })[1];
                }

                TextoVario.Text = "Progreso:";
                TextoSelectHypervisor.Visible = false;
                TextoSelectVersion.Visible = false;
                SelectHypervisor.Visible = false;
                SelectVersion.Visible = false;
                BotonConvertir.Visible = false;
                SelectArchivo.Visible = false;
                BarraProgreso.Visible = true;
                TextLog.Visible = true;
                TextLog.Text = "";
                BotonAtras.Visible = true;
                BotonAtras.Enabled = false;
                BotonSalir.Visible = true;
                BotonSalir.Enabled = false;

                JObject configMVFinal = null;

                conf.getConfiguracion(SelectArchivo.Text);

                foreach (KeyValuePair<string, JObject> Par in conf.Config)
                {
                    if (Par.Key.Equals(SelectHypervisor.Text))
                    {
                        configMVFinal = Par.Value;
                        break;
                    }
                }

                crear.crearCarpeta(configMVFinal);
                List<string> rutasHDDFinal = crear.convertirDiscosDuros(configMVFinal);

                conf.SO = getSOFinal(conf.ConfigMV["OSTypes"].ToArray(), conf.SO, configMVFinal["OSTypes"].ToArray());
                crear.crearMV(configMVFinal, rutasHDDFinal);
            }
        }

        private string getSOFinal(JToken[] OStypesIni, string so, JToken[] OStypesFin)
        {
            int contador = 0;
            foreach (var OSType in OStypesIni)
            {
                if (OSType.ToString().Equals(so))
                {
                    break;
                }
                else
                {
                    contador++;
                }
            }

            if (OStypesFin[contador].ToString().Equals("32"))
            {
                return OStypesFin[OStypesFin.Length - 2].ToString();
            }
            else if (OStypesFin[contador].ToString().Equals("64"))
            {
                return OStypesFin.Last().ToString();
            }
            else
            {
                return OStypesFin[contador].ToString();
            }

        }

        public void SetLog(string info)
        {
            if (this.TextLog.InvokeRequired)
            {
                log d = new log(SetLog);
                this.Invoke(d, new object[] { info });
            }
            else
            {
                this.TextLog.AppendText("- " + info);
            }
        }

        public void actualizarBarra(string progreso)
        {
            if (progreso.Contains("/100"))
            {
                int prog = int.Parse(progreso.Split(new string[] { "/100" }, StringSplitOptions.None)[0].Split(new string[] { "(" }, StringSplitOptions.None)[1].Split(new string[] { "." }, StringSplitOptions.None)[0]);
                SetProgressBarValue(prog);
            }

        }

        private void SetProgressBarValue(int prog)
        {
            if (this.BarraProgreso.InvokeRequired)
            {
                progresoBarra d = new progresoBarra(SetProgressBarValue);
                this.Invoke(d, new object[] { prog });
            }
            else
            {
                this.BarraProgreso.Value = prog;
                TextoVario.Text = "Progreso:" + prog + "%";
                
            }
        }

        public void SetActivarBotones(bool habilitar)
        {
            if (this.BotonAtras.InvokeRequired && this.BotonSalir.InvokeRequired)
            {
                habilitarBotones d = new habilitarBotones(SetActivarBotones);
                this.Invoke(d, new object[] { habilitar });
            }
            else
            {
                this.BotonAtras.Enabled = habilitar;
                this.BotonSalir.Enabled = habilitar;
            }
        }

        private void botonAtras_Click(object sender, EventArgs e)
        {
            BarraProgreso.Value = 0;
            BarraProgreso.Visible = false;
            SelectArchivo.Text = "";
            SelectArchivo.Visible = true;
            BotonAtras.Visible = false;
            BotonSalir.Visible = false;
            BotonConvertir.Visible = true;
            TextoSelectHypervisor.Visible = true;
            SelectHypervisor.SelectedIndex = 0;
            selectHypervisor_SelectedIndexChanged(null, null);
            SelectHypervisor.Visible = true;
            TextoVario.Text = "Seleccione el archivo de configuración de la MV (.vmx, .vbox, ...)";
            TextoVario.Visible = true;
            TextLog.SaveFile("log\\log_" + System.DateTime.Now.ToString().Replace("/", "-").Replace(":", "_")+".rtf");
            TextLog.Visible = false;
            
        }

        private void botonSalir_Click(object sender, EventArgs e)
        {
            TextLog.SaveFile("log\\log_" + System.DateTime.Now.ToString().Replace("/", "-").Replace(":", "_")+".rtf");
            Application.Exit();
        }

        public Label TextoVario
        {
            get
            {
                return textoVario;
            }

            set
            {
                textoVario = value;
            }
        }

        public TextBox SelectArchivo
        {
            get
            {
                return selectArchivo;
            }

            set
            {
                selectArchivo = value;
            }
        }

        public Label TextoSelectHypervisor
        {
            get
            {
                return textoSelectHypervisor;
            }

            set
            {
                textoSelectHypervisor = value;
            }
        }

        public ComboBox SelectHypervisor
        {
            get
            {
                return selectHypervisor;
            }

            set
            {
                selectHypervisor = value;
            }
        }

        public ComboBox SelectVersion
        {
            get
            {
                return selectVersion;
            }

            set
            {
                selectVersion = value;
            }
        }

        public Label TextoSelectVersion
        {
            get
            {
                return textoSelectVersion;
            }

            set
            {
                textoSelectVersion = value;
            }
        }

        public Button BotonConvertir
        {
            get
            {
                return botonConvertir;
            }

            set
            {
                botonConvertir = value;
            }
        }

        public ProgressBar BarraProgreso
        {
            get
            {
                return barraProgreso;
            }

            set
            {
                barraProgreso = value;
            }
        }

        public Button BotonAtras
        {
            get
            {
                return botonAtras;
            }

            set
            {
                botonAtras = value;
            }
        }

        public Button BotonSalir
        {
            get
            {
                return botonSalir;
            }

            set
            {
                botonSalir = value;
            }
        }

        public RichTextBox TextLog
        {
            get
            {
                return textLog;
            }

            set
            {
                textLog = value;
            }
        }
    }
}
